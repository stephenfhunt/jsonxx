#ifndef JSONXX_PARSER_H
#define JSONXX_PARSER_H

#include <istream>
#include <string>

#include "Data.h"

namespace jsonxx
{
    /// The callback interface used to provide clients 
    /// information about the parse
    struct ParseHandler
    {
        virtual ~ParseHandler() {}

        /// An integer inside an Object, with its key text
        virtual void onInteger(const std::string& name, int value) { }

        /// An integer outside an Object
        virtual void onInteger(int value) { }

        /// A floating point inside an Object, with its key text
        virtual void onDouble(const std::string& name, double value) { }

        /// A floating point outside an Object
        virtual void onDouble(double value) { }

        /// A String inside an Object, with its key text
        virtual void onString(const std::string& name, const std::string& value) { }

        /// A String outside an Object
        virtual void onString(const std::string& value) { }

        /// A Boolean inside an Object, with its key text
        virtual void onBoolean(const std::string& name, bool value) { }

        /// A Boolean outside an Object
        virtual void onBoolean(bool value) { }

        /// A null inside an Object
        virtual void onNull(const std::string& name) { }

        /// A null outside an Object
        virtual void onNull() { }

        /// Called at the start of an array inside an object
        virtual void arrayStart(const std::string& name) { }

        /// Called at the start of an array outside an object
        virtual void arrayStart() { }

        /// Called at the end of an array
        virtual void arrayEnd() { }

        /// Called at the start of an object nested inside another
        virtual void objectStart(const std::string& name) { }

        /// Called at the start of an object not nested in another
        virtual void objectStart() { }

        /// Called at the end of an object
        virtual void objectEnd() { }
    };

    enum ParseError
    {
        SUCCESS,                    /// everything is just fine
        INVALID_JSON_DATA,          /// bogus data in document
        INVALID_DATA_SOURCE,        /// can't read from input at all
        EXCESS_DATA_AFTER_FINAL,    /// data was received after a document 
        /// has been completed
        OUT_OF_MEMORY,
    };

    /**
     * Parse an in-memory JSON document.  
     * @param data a complete JSON document ready for parsing
     * @param length byte length of the document
     * @param handler to receive callbacks about the data
     * @return a ParseError indicating the success or failure of the parse
     */
    ParseError parse(const char* data, unsigned int length, ParseHandler& handler);

    /// Parses and nul-terminated C string
    ParseError parse(const std::string& s, ParseHandler& handler);

    /// Run the parser, reading from the given istream 
    ParseError parse(std::istream& input, ParseHandler& handler);

    /**
     * Parse an in-memory JSON document.  
     * @param data a complete JSON document ready for parsing
     * @param length byte length of the document
     * @return a Value object on successful parse, NULL otherwise
     */
    std::unique_ptr<Value> parse(const char* data, unsigned int length);

    /// Run the parser on a string of expected JSON 
    std::unique_ptr<Value> parse(const std::string& s);

    /// Run the parser, reading from the given istream 
    std::unique_ptr<Value> parse(std::istream& input);

    /// Generate a human-readable representation of a parse return code
    const char* getErrorString(ParseError error);

} 

#endif 
