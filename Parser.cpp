#include <string>
#include <sstream>

#include "Parser.h"
#include "ValueHandler.h"

namespace jsonxx
{

    // utility function for converting a unicode codepoint 
    // to a sequence of bytes
    std::string codepointToUtf8(uint32_t codepoint)
    {
        // work out number of bytes required for this character
        unsigned int lengthTable[] = { 0, 0x80, 0x800, 0x10000, 0x200000 };
        unsigned int outputLength(0);
        for (unsigned int i=0; i < sizeof(lengthTable)/sizeof(lengthTable[0]); ++i)
        {
            if (codepoint < lengthTable[i]) 
            {
                outputLength = i;
                break;
            }
        }

        // not a valid character
        if (outputLength < 1 || outputLength > 4) return "";

        std::string utf8;

        // secondary bytes are the same no matter the length
        if (outputLength > 1) utf8 = 0x80 | (codepoint & 0x3F);
        if (outputLength > 2) utf8.insert(utf8.begin(), 0x80 | (codepoint >> 6 & 0x3F));
        if (outputLength > 3) utf8.insert(utf8.begin(), 0x80 | (codepoint >> 12 & 0x3f)); 

        // the leading byte differs based on length
        char leading(0);
        switch (outputLength)
        {
        case 1:
            leading = codepoint & 0xFF;
            break;
        case 2:
            leading = 0xC0 | (codepoint >> 6);
            break;
        case 3:
            leading = 0xE0 | (codepoint >> 12);
            break;
        case 4:
            leading = 0xF0 | (codepoint >> 18);
            break;
        }

        return utf8.insert(0, 1, leading);
    }

    /**
     * A single parse instance
     */
    class Parse
    {
    public:

        Parse(std::istream& input,
              jsonxx::ParseHandler& handler)
            : _input(input), 
              _nextByte(0),
              _handler(handler),
              _error(jsonxx::SUCCESS)
        {
            readNextByte();
        }

        jsonxx::ParseError run()
        {
            expect(value(NULL));

            // make sure there is no remaining data after the value
            if (_hasNextByte) _error = jsonxx::EXCESS_DATA_AFTER_FINAL;

            return _error;
        }

    private:

// utility macros for making handler calls
#define callHandler(callback, name, value) { \
    if (name) _handler.callback(name, value); \
    else _handler.callback(value); \
}

#define callHandlerNoValue(callback, name) { \
    if (name) _handler.callback(name); \
    else _handler.callback(); \
}

        //////////////////////////////////////////////////////////////////////
        // The grammar (see http://json.org for reference)
        /////////////////////////////////////////////////////////////////////

        bool value(const char* name)
        {
            whitespace();
            bool valid = object(name) || array(name) || string(name) || number(name) ||
                         literalTrue(name) || literalFalse(name) || literalNull(name);
            whitespace();
            return valid;
        }

        bool string(const char* name)
        {
            std::string v;
            v.reserve(32);

            if (commonString(v))
            {
                callHandler(onString, name, v);
                return true;
            }

            return false;
        }

        inline bool commonString(std::string& s)
        {
            whitespace();
            if (accept('\"'))
            {
                chars(s);
                bool valid = expectChar('\"');
                whitespace();
                return valid;
            }

            return false;
        }

        inline void chars(std::string& s)
        {
            while (jschar(s)) ;
        }

        inline bool jschar(std::string& s)
        {
            if (!_hasNextByte) return false;
            if (_nextByte == '\"') return false;

            char c = _nextByte;

            // handle escape characters
            if (_nextByte == '\\')
            {
                readNextByte();
                if (!_hasNextByte) return false;

                c = _nextByte;

                switch (c)
                {
                    case 'b': c = '\b'; break;
                    case 'f': c = '\f'; break;
                    case 'n': c = '\n'; break;
                    case 'r': c = '\r'; break;
                    case 't': c = '\t'; break;
                    // Because unicode codepoints require more than
                    // one additional byte, we handle them completely here,
                    // returning immediately after parsing the character
                    // rather than proceeding through the rest of the function
                    case 'u':
                    {
                        readNextByte();
                        std::string codetext;
                        expect(hexChar(codetext));
                        expect(hexChar(codetext));
                        expect(hexChar(codetext));
                        expect(hexChar(codetext));
                        uint32_t codepoint;
                        if (1 == sscanf(codetext.c_str(), "%x", &codepoint))
                        {
                            s += codepointToUtf8(codepoint);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                        break;
                    }
                }
            }

            s += c;
            readNextByte();
            return true;
        }

        bool hexChar(std::string& hexDigits)
        {
            if (_hasNextByte &&
                ((_nextByte >= '0' && _nextByte <= '9') ||
                 (_nextByte >= 'A' && _nextByte <= 'F') ||
                 (_nextByte >= 'a' && _nextByte <= 'f')))
            {
                hexDigits += _nextByte;
                readNextByte();
                return true;
            }

            return false;
        }

        bool number(const char* name)
        {
            std::string nstr;
            nstr.reserve(8);

            if (!integer(nstr)) return false;

            bool isFloat = frac(nstr);
            bool hasExp = exp(nstr);
            isFloat = isFloat || hasExp;

            if (isFloat)
            {
                double d;
                if (1 == sscanf(nstr.c_str(), "%lf", &d))
                {
                    callHandler(onDouble, name, d);
                    return true;
                }
            }
            else
            {
                int i;
                if (1 == sscanf(nstr.c_str(), "%d", &i))
                {
                    callHandler(onInteger, name, i);
                    return true;
                }
            }

            return false;
        }

        inline bool integer(std::string& nstr)
        {
            std::string backup(nstr);
            if (accept('-')) nstr += '-';

            if (!digits(nstr))
            {
                nstr = backup;
                return false;
            }

            return true;

        }

        inline bool digits(std::string& nstr)
        {
            bool success=_hasNextByte && _nextByte >= '0' && _nextByte <= '9';

            while (_hasNextByte && _nextByte >= '0' && _nextByte <= '9')
            {
                nstr += _nextByte;
                readNextByte();
            }

            return success;
        }

        inline bool frac(std::string& nstr)
        {
            std::string digitText;
            if (accept('.') && expect(digits(digitText)))
            {
                nstr += '.';
                nstr += digitText;
                return true;
            }

            return false;
        }

        inline bool exp(std::string& nstr)
        {
            if (accept('e') || accept('E'))
            {
                std::string digitText("e");
                if (accept('+')) digitText += '+';
                else if (accept('-')) digitText += '-';

                if (expect(digits(digitText)))
                {
                    nstr += digitText;
                    return true;
                }
            }

            return false;
        }

        bool object(const char* name)
        {
            if (accept('{'))
            {
                callHandlerNoValue(objectStart, name);

                whitespace();

                // handle empty objects
                if (accept('}'))
                {
                    _handler.objectEnd();
                    return true;
                }

                members();

                if (expectChar('}'))
                {
                    _handler.objectEnd();
                    return true;
                }
            }

            return false;
        }

        inline void members()
        {
            do
            {
                pair();
            }
            while (accept(','));
        }

        inline bool pair()
        {
            std::string k;
            k.reserve(16);

            return expect(commonString(k)) &&
                   expectChar(':') &&
                   expect(value(k.c_str()));
        }

        bool array(const char* name)
        {
            if (accept('['))
            {
                callHandlerNoValue(arrayStart, name);

                whitespace();

                // handle empty array
                if (accept(']'))
                {
                    _handler.arrayEnd();
                    return true;
                }

                elements();

                if (expectChar(']'))
                {
                    _handler.arrayEnd();
                    return true;
                }
            }

            return false;
        }

        inline void elements()
        {
            do
            {
                expect(value(NULL));
            }
            while (accept(','));
        }

        bool literalTrue(const char* name)
        {
            if (accept('t'))
            {
                bool good = expectChar('r') &&
                            expectChar('u') &&
                            expectChar('e');
                if (good)
                {
                    callHandler(onBoolean, name, true);
                    return true;
                }
            }

            return false;
        }

        bool literalFalse(const char* name)
        {
            if (accept('f'))
            {
                bool good = expectChar('a') &&
                            expectChar('l') &&
                            expectChar('s') &&
                            expectChar('e');
                if (good)
                {
                    callHandler(onBoolean, name, false);
                    return true;
                }
            }

            return false;
        }

        bool literalNull(const char* name)
        {
            if (accept('n'))
            {
                bool good = expectChar('u') &&
                            expectChar('l') &&
                            expectChar('l');
                if (good)
                {
                    callHandlerNoValue(onNull, name);
                    return true;
                }
            }

            return false;
        }

        inline void whitespace()
        {
            while (accept(' ') || accept('\n') ||
                   accept('\t') || accept('\r')) ;
        }

        ///////////////////////////////////////////////////////////////////////
        // Utility functions for the parsing process
        //////////////////////////////////////////////////////////////////////

        void readNextByte()
        {
            _input.get(_nextByte);
            _hasNextByte = _input.gcount() > 0;
        }

        inline bool accept(char c)
        {
            if (_hasNextByte && _nextByte == c)
            {
                readNextByte();
                return true;
            }

            return false;
        }

        bool expectChar(char c)
        {
            bool result = accept(c);
            if (!result)
            {
                _error = jsonxx::INVALID_JSON_DATA;
                _hasNextByte = false; // stop reading after first error
            }
            return result;
        }

        bool expect(bool b)
        {
            if (!b)
            {
                _error = jsonxx::INVALID_JSON_DATA;
                _hasNextByte = false; // stop reading after first error
            }
            return b;
        }

        /////////////////////////////////////////////////////////////////
        // Parse state data
        ////////////////////////////////////////////////////////////////

        std::istream& _input;

        char _nextByte;

        bool _hasNextByte;

        jsonxx::ParseHandler& _handler;

        jsonxx::ParseError _error;
    };

    ParseError parse(const char* data, unsigned int length, ParseHandler& handler)
    {
        if (!data) return INVALID_DATA_SOURCE;

        // since JSON must be valid string text, NULL bytes may
        // not be present in the input, so constructing a string
        // out of the input is safe
        return parse(std::string(data, length), handler);
    }


    ParseError parse(std::istream& input, ParseHandler& handler)
    {
        if (!input) return INVALID_DATA_SOURCE;

        Parse parseRun(input, handler);
        return parseRun.run();
    }

    const char* getErrorString(ParseError error) 
    {
        switch(error)
        {
            case SUCCESS: return "Success";
            case INVALID_JSON_DATA: return "Invalid json data";
            case INVALID_DATA_SOURCE: return "Invalid data source";
            case EXCESS_DATA_AFTER_FINAL: return "Excess data after json";
            case OUT_OF_MEMORY: return "Out of memory";
        }
        return "";
    }

    ParseError parse(const std::string& s, ParseHandler& handler)
    {
        std::istringstream stream(s);
        return parse(stream, handler);
    }

    std::unique_ptr<Value> parse(const char* data, unsigned int length)
    {
        ValueHandler handler;        
        std::unique_ptr<Value> result;
        if (SUCCESS == parse(data, length, handler))
            result = handler.value(); 

        return result;
    }

    std::unique_ptr<Value> parse(const std::string& s)
    {
        ValueHandler handler;        
        std::unique_ptr<Value> result;
        if (SUCCESS == parse(s, handler))
            result = handler.value(); 

        return result;
    }

    std::unique_ptr<Value> parse(std::istream& input)
    {
        ValueHandler handler;        
        std::unique_ptr<Value> result;
        if (SUCCESS == parse(input, handler))
            result = handler.value(); 

        return result;
    }
} 

