#ifndef _JSONXX_DATA_H_
#define _JSONXX_DATA_H_

#include <memory>
#include <string>
#include <vector>
#include <map>

namespace jsonxx
{
    class Object;
    class Array;
    class Number;

    /**
     * The common type of all JSON values
     */
    class Value
    {
    public:
        virtual ~Value() {}

        /**
         * Interpret this value as an object
         * @param o an Object pointer to populate
         * @return true if the value is an object, false otherwise
         * @post if an object, o is populated; if not it's unchanged
         */
        virtual bool as(Object*& o) { return false; }

        /**
         * Interpret this value as an array 
         * @param a an Array pointer to populate
         * @return true if the value is an array, false otherwise
         * @post if an array, a is populated; if not it's unchanged
         */
        virtual bool as(Array*& a) { return false; }

        /**
         * Interpret this value as a number
         * @param n a Number pointer to populate
         * @return true if the value is a number, false otherwise
         * @post if a number, n is populated; if not it's unchanged
         */
        virtual bool as(Number*& n) { return false; }

        /**
         * Interpret this value as an int 
         * @param n a An int to populate
         * @return true if the value is an int, false otherwise
         * @post if an int, n is populated; if not it's unchanged
         */
        virtual bool as(int& n) { return false; }

        /**
         * Interpret this value as a double
         * @param n A double to populate
         * @return true if the value is an double, false otherwise
         * @post if a double, n is populated; if not it's unchanged
         */
        virtual bool as(double& n) { return false; }

        /**
         * Interpret this value as a boolean
         * @param b a bool reference to populate
         * @return true if the value is a boolean, false otherwise
         * @post if a boolean, b is populated; if not it's unchanged
         */
        virtual bool as(bool& b) { return false; }

        /**
         * Interpret this value as a string 
         * @param s a std::string reference to populate
         * @return true if the value is a string, false otherwise
         * @post if a string, b is populated; if not it's unchanged
         */
        virtual bool as(std::string& s) { return false; }

        /**
         * @return true if this JSON value is the null literal
         */
        virtual bool isNull() { return false; }

        /**
         * The JSON textual representation for this value
         * @return a string of JSON data for this value
         */
        virtual std::string representation() const = 0;
        
    };

    /// Allow writing JSON formats of Values to streams
    std::ostream& operator<<(std::ostream& o, const Value& v);

    /**
     * A JSON object (string key -> Value pairs)
     */
    class Object : public Value
    {
    public:

        bool as(Object*& o);

        Value& operator[](const std::string& k);

        /// @return true if there is an item of the given key in this
        bool has(const std::string& k);

        /// @return the number of items in this Object 
        unsigned int size() const { return storage.size(); }

        /// insert a key/value pair into this Object
        void insert(const std::string& key, std::unique_ptr<Value> value);

        /// insert a string value
        void insert(const std::string& key, const std::string& value);

        /// insert an int Number value
        void insert(const std::string& key, int value);

        /// insert a double Number value
        void insert(const std::string& key, double value);

        /// insert a Boolean value
        void insert(const std::string& key, bool value);

        /// insert a null value 
        void insertNull(const std::string& key);

        /** 
         * Interpret a homogeneous JSON object as a std::map
         * @param m a map to populate with the contents of *this
         * @return true if the value is an object and all items can
         *              be interpreted as the template type
         * @post m is populated if true is returned, unchanged if false
         */
        template <typename T>
        bool as(std::map<std::string, T>& m)
        {
            std::map<std::string, T> working;
            for (std::map<std::string, std::unique_ptr<Value> >::iterator i=storage.begin();
                 i != storage.end();
                 ++i)
            {
                T value;
                if (i->second->as(value))
                    working.insert(i->first, value);
                else return false;
            }

            m = working;
            return true;
        }

        std::string representation() const;

    private:

        std::map<std::string, std::unique_ptr<Value> > storage;

    };

    /**
     * A JSON array
     * An ordered list of values
     */
    class Array : public Value
    {
    public:

        bool as(Array*& a);

        /// access an Array item by index
        Value& operator[](unsigned int i);

        /// @return the number of items in this Array
        unsigned int size() const { return storage.size(); }

        /// append a new value to the end of this Array
        void add(std::unique_ptr<Value> newValue);

        /// append a new string to the end of this array
        void add(const std::string& value);

        /// append a new Number to the end of this array
        void add(int value);

        /// append a new Number to the end of this array
        void add(double value);

        /// append a new Boolean to the end of this array
        void add(bool value);

        /// append a null to the end of this array
        void addNull();

        /** 
         * Interpret a homogeneous JSON array as a std::vector
         * @param v a vector to populate with the contents of *this
         * @return true if the value is an array and all items can
         *              be interpreted as the template type
         * @post v is populated if true is returned, unchanged if false
         */
        template <typename T>
        bool as(std::vector<T>& v)
        {
            std::vector<T> working;
            working.reserve(storage.size());
            for (std::vector<std::unique_ptr<Value> >::iterator i = storage.begin();
                 i != storage.end();
                 ++i)
            {
                T item;
                if ((*i)->as(item))
                {
                    working.push_back(item);
                }
                else
                {
                    return false;
                }
            }

            v = working;
            return true;
        }

        std::string representation() const;

    private:

        std::vector<std::unique_ptr<Value> > storage;
    };

    /**
     * JSON's literal true and false values
     */
    class Boolean : public Value
    {
    public:

        Boolean(bool b) : storage(b) {}

        bool as(bool& b);

        std::string representation() const;

    private:
     
        bool storage;

    };

    /**
     * JSON's numeric type
     */
    class Number : public Value
    {
    public:

        Number(int i);

        Number(double d);
        
        bool as(Number*& n);

        /**
         * Interpret this Number as an integer
         * @param n an int to populate
         * @return true if this is an integer value
         * @post if an int, n is populated; if not, it is unchanged
         */
        bool as(int& n);

        /**
         * Interpret this Number as a double
         * @param n a double to populate
         * @return true if this is a floating point value
         * @post if floating point, n is populated; if not, it is unchanged
         */
        bool as(double& n);

        /**
         * Interpet this Number as an int
         * Unlike as(), does type coercion to int if the value 
         * is actually a floating point 
         */
        int asInt() const;

        /**
         * Interpet this Number as a double 
         * Unlike as(), does type coercion to double if the value 
         * is actually an integer
         */
        double asDouble() const;

        std::string representation() const;

    private:

        struct Storage
        {
            enum 
            {
                INT,
                DOUBLE
            } type;

            union
            {
                int intValue;
                double doubleValue;
            } value;
        } storage;

    };

    /** 
     * A JSON string literal
     */
    class String : public Value
    {
    public:

        String(std::string s);

        bool as(std::string& s);

        std::string representation() const;

    private:

        std::string storage;

    };

    /**
     * JSON's literal null
     */
    class Null : public Value
    {
    public:
        
        bool isNull() { return true; }

        std::string representation() const { return "null"; }
    };

}

#endif

