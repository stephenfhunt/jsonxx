#include "Data.h"

#include <cassert>
#include <sstream>

void verify(jsonxx::Value& v, const std::string& expected)
{
    if (v.representation() != expected)
    {
        printf("Expected representation %s, got %s\n", expected.c_str(), v.representation().c_str());
        assert(false);
    }
    
    std::ostringstream stream;
    stream << v;
    if (stream.str() != expected)
    {
        printf("Expected streamed %s, got %s\n", expected.c_str(), stream.str().c_str());
        assert(false);
    }
}

void testNumbers()
{
#define TEST(n, s) { jsonxx::Number v(n); verify(v, s); }
    TEST(0, "0"); 
    TEST(-1, "-1");
    TEST(1, "1");
    TEST(0.0, "0");
    TEST(1.1, "1.1");
    TEST(-0.3, "-0.3");
    TEST(3.14, "3.14");
    TEST(213.47, "213.47");
#undef TEST
}

void testStrings()
{
#define TEST(s, e) { jsonxx::String v(s); verify(v, e); }
    TEST("", "\"\"");
    TEST("a", "\"a\"");
    TEST("abc", "\"abc\"");
    TEST("    ", "\"    \"");
    TEST("\n", "\"\\n\"");
    TEST("\n\t\b\f\r\\", "\"\\n\\t\\b\\f\\r\\\\\"");
#undef TEST
}

void testLiterals()
{
    jsonxx::Boolean t(true);
    jsonxx::Boolean f(false);
    jsonxx::Null n;
    verify(t, "true");
    verify(f, "false");
    verify(n, "null");
}

void testObjects()
{
    { 
        jsonxx::Object o;
        verify(o, "{}");
    }

    { 
        jsonxx::Object o;
        o.insert("key", std::string("value"));
        verify(o, "{\"key\":\"value\"}");
    }

    { 
        jsonxx::Object o;
        o.insertNull("key");
        verify(o, "{\"key\":null}");
    }

    { 
        jsonxx::Object o;
        o.insert("true", true);
        o.insert("false", false);
        verify(o, "{\"false\":false,\"true\":true}");
    }

    {
        jsonxx::Object o;
        o.insert("pi", 3.14);
        o.insert("one", 1);
        verify(o, "{\"one\":1,\"pi\":3.14}");
    }

    {
        jsonxx::Object o;
        std::unique_ptr<jsonxx::Value> a(new jsonxx::Array);
        o.insert("array", std::move(a));
        verify(o, "{\"array\":[]}");
    }

    {
        jsonxx::Object o;
        std::unique_ptr<jsonxx::Value> a(new jsonxx::Array);
        jsonxx::Array* aa;
        a->as(aa);
        aa->add(std::string("abc"));
        o.insert("array", std::move(a));
        verify(o, "{\"array\":[\"abc\"]}");
    }

    {
        jsonxx::Object o;
        std::unique_ptr<jsonxx::Value> inner(new jsonxx::Object);
        o.insert("inner", std::move(inner));
        verify(o, "{\"inner\":{}}");
    }

    {
        jsonxx::Object o;
        std::unique_ptr<jsonxx::Value> inner(new jsonxx::Object);
        jsonxx::Object* innerO; inner->as(innerO);
        innerO->insert("int", 5);
        o.insert("inner", std::move(inner));
        verify(o, "{\"inner\":{\"int\":5}}");
    }
}

void testArrays()
{
    { 
        jsonxx::Array a;
        verify(a, "[]");
    }

    { 
        jsonxx::Array a;
        a.add(std::string("value"));
        verify(a, "[\"value\"]");
    }

    { 
        jsonxx::Array a;
        a.addNull();
        verify(a, "[null]");
    }

    { 
        jsonxx::Array a;
        a.add(true);
        a.add(false);
        verify(a, "[true,false]");
    }

    {
        jsonxx::Array a;
        a.add(3.14);
        a.add(1);
        verify(a, "[3.14,1]");
    }

    {
        jsonxx::Array a;
        std::unique_ptr<jsonxx::Value> ia(new jsonxx::Array);
        a.add(std::move(ia));
        verify(a, "[[]]");
    }

    {
        jsonxx::Array a;
        std::unique_ptr<jsonxx::Value> ia(new jsonxx::Array);
        jsonxx::Array* aa;
        ia->as(aa);
        aa->add(std::string("abc"));
        a.add(std::move(ia));
        verify(a, "[[\"abc\"]]");
    }

    {
        jsonxx::Array a;
        std::unique_ptr<jsonxx::Value> inner(new jsonxx::Object);
        a.add(std::move(inner));
        verify(a, "[{}]");
    }

    {
        jsonxx::Array a;
        std::unique_ptr<jsonxx::Value> inner(new jsonxx::Object);
        jsonxx::Object* innerO; inner->as(innerO);
        innerO->insert("int", 5);
        a.add(std::move(inner));
        verify(a, "[{\"int\":5}]");
    }
}

int main()
{
    testNumbers();
    testStrings();
    testLiterals();
    testObjects();
    testArrays();
    return 0;
}

