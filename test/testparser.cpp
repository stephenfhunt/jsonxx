#include "Parser.h"

#include <cassert>
#include <sstream>

void testStringInput()
{
    jsonxx::ParseHandler nullHandler;
#define TEST(e, s) assert(jsonxx::e == jsonxx::parse(s, nullHandler));
    TEST(SUCCESS, "\"abc\"");
    TEST(SUCCESS, "3");
    TEST(SUCCESS, "3.14");
    TEST(SUCCESS, "null");
    TEST(SUCCESS, "true");
    TEST(SUCCESS, "false");
    TEST(SUCCESS, "[]");
    TEST(SUCCESS, "[[]]");
    TEST(SUCCESS, "{}");
    TEST(SUCCESS, "{\"inner\":{}}");
    TEST(SUCCESS, "[{}]");
    TEST(SUCCESS, "{\"inner\":[]}");
    TEST(SUCCESS, "{\"inner\":[1, \"abc\", 3.14, \"def\"]}");

    TEST(INVALID_JSON_DATA, "abc");
    TEST(EXCESS_DATA_AFTER_FINAL, "3abc");
    TEST(INVALID_JSON_DATA, "3.abc");
    TEST(EXCESS_DATA_AFTER_FINAL, "3.1abc");
    TEST(INVALID_JSON_DATA, "tru");
    TEST(INVALID_JSON_DATA, "fals");
    TEST(INVALID_JSON_DATA, "nul");
    TEST(INVALID_JSON_DATA, "(");
    TEST(INVALID_JSON_DATA, ")");
    TEST(INVALID_JSON_DATA, "{");
    TEST(INVALID_JSON_DATA, "}");
    TEST(INVALID_JSON_DATA, "[1, 2, 3, 4,]");
    TEST(INVALID_JSON_DATA, "{\"one\":1, \"two\":2, \"three\"3,}");
    TEST(INVALID_JSON_DATA, "[1, 2, 3, 4}");
    TEST(INVALID_JSON_DATA, "{\"one\":1, \"two\":2, \"three\"3]");
#undef TEST
}

void testStreamInput()
{
    jsonxx::ParseHandler nullHandler;
    std::istringstream stream;
#define TEST(e, s) { \
    stream.str(s); \
    stream.clear(); \
    assert(jsonxx::e == jsonxx::parse(stream, nullHandler)); \
}
    TEST(SUCCESS, "\"abc\"");
    TEST(SUCCESS, "3");
    TEST(SUCCESS, "3.14");
    TEST(SUCCESS, "null");
    TEST(SUCCESS, "true");
    TEST(SUCCESS, "false");
    TEST(SUCCESS, "[]");
    TEST(SUCCESS, "[[]]");
    TEST(SUCCESS, "{}");
    TEST(SUCCESS, "{\"inner\":{}}");
    TEST(SUCCESS, "[{}]");
    TEST(SUCCESS, "{\"inner\":[]}");
    TEST(SUCCESS, "{\"inner\":[14]}");
    TEST(SUCCESS, "{\"inner\":[1, 2, 3, 4]}");

    TEST(INVALID_JSON_DATA, "abc");
    TEST(EXCESS_DATA_AFTER_FINAL, "3abc");
    TEST(INVALID_JSON_DATA, "3.abc");
    TEST(EXCESS_DATA_AFTER_FINAL, "3.1abc");
    TEST(INVALID_JSON_DATA, "tru");
    TEST(INVALID_JSON_DATA, "fals");
    TEST(INVALID_JSON_DATA, "nul");
    TEST(INVALID_JSON_DATA, "(");
    TEST(INVALID_JSON_DATA, ")");
    TEST(INVALID_JSON_DATA, "{");
    TEST(INVALID_JSON_DATA, "}");
    TEST(INVALID_JSON_DATA, "[1, 2, 3, 4,]");
    TEST(INVALID_JSON_DATA, "{\"one\":1, \"two\":2, \"three\"3,}");;
    TEST(INVALID_JSON_DATA, "[1, 2, 3, 4}");
    TEST(INVALID_JSON_DATA, "{\"one\":1, \"two\":2, \"three\"3]");;
#undef TEST
}

void testValues()
{
#define TEST_SUCCESS(input, check) { \
    assert(check(*jsonxx::parse(input))); \
}
    TEST_SUCCESS("\"abc\"", [] (jsonxx::Value& v) -> bool 
    { 
        std::string s; 
        return v.as(s) && s == "abc"; 
    } );

    TEST_SUCCESS("3", [] (jsonxx::Value& v) -> bool
    {
        int i;
        return v.as(i) && i == 3;
    } );

    TEST_SUCCESS("3.14", [] (jsonxx::Value& v) -> bool
    {
        double d;
        return v.as(d) && d == 3.14;
    } );

    TEST_SUCCESS("null", [] (jsonxx::Value& v) { return v.isNull(); });

    TEST_SUCCESS("true", [] (jsonxx::Value& v) -> bool
    {
        bool b;
        return v.as(b) && b;
    } );

    TEST_SUCCESS("false", [] (jsonxx::Value& v) -> bool
    {
        bool b;
        return v.as(b) && !b;
    } );

    TEST_SUCCESS("[]", [] (jsonxx::Value& v) -> bool
    {
        jsonxx::Array* a;
        return v.as(a) && a->size() == 0;
    } );

    TEST_SUCCESS("[[]]", [] (jsonxx::Value& v) -> bool
    {
        jsonxx::Array* a;
        if (v.as(a) && a->size() == 1)
        {
            jsonxx::Array* inner;
            return (*a)[0].as(inner) && inner->size() == 0;
        }
        
        return false;
    } );

    TEST_SUCCESS("{}", [] (jsonxx::Value& v) -> bool
    {
        jsonxx::Object* o; 
        return v.as(o) && o->size() == 0;
    } );

    TEST_SUCCESS("{\"inner\":{}}", [] (jsonxx::Value& v) -> bool
    {
        jsonxx::Object* o; 
        if (v.as(o) && o->size() == 1)
        {
            jsonxx::Object* inner;
            return o->has("inner") && 
                   (*o)["inner"].as(inner) &&
                   inner->size() == 0;
        }
        
        return false;
    } );

    TEST_SUCCESS("[{}]", [] (jsonxx::Value& v) -> bool
    {
        jsonxx::Array* a;
        if (v.as(a) && a->size() == 1)
        {
            jsonxx::Object* o;
            return (*a)[0].as(o) && o->size() == 0;
        }

        return false;
    
    } );

    TEST_SUCCESS("{\"inner\":[]}", [] (jsonxx::Value& v) -> bool
    {
        jsonxx::Object* o;
        if (v.as(o) && o->size() == 1 && o->has("inner"))
        {
            jsonxx::Array* a;
            return (*o)["inner"].as(a) && a->size() == 0;
        }

        return false;
    } );

    TEST_SUCCESS("{\"inner\":[14]}", [] (jsonxx::Value& v) -> bool
    {
        jsonxx::Object* o;
        if (v.as(o) && o->size() == 1 && o->has("inner"))
        {
            jsonxx::Array* a;
            if ((*o)["inner"].as(a) && a->size() == 1)
            {
                int i;
                return (*a)[0].as(i) && i == 14;
            }
        }

        return false;
    } );

    TEST_SUCCESS("{\"inner\":[1, 2, 3, 4]}", [] (jsonxx::Value& v) -> bool
    {
        jsonxx::Object* o;
        if (v.as(o) && o->size() == 1 && o->has("inner"))
        {
            jsonxx::Array* a;
            if ((*o)["inner"].as(a) && a->size() == 4)
            {
                int i;
                return (*a)[0].as(i) && i == 1 &&
                       (*a)[1].as(i) && i == 2 &&
                       (*a)[2].as(i) && i == 3 &&
                       (*a)[3].as(i) && i == 4;
            }
        }

        return false;
    } );

    TEST_SUCCESS("{\"first\": 25, \"second\": \"abc\"}", [] (jsonxx::Value& v) -> bool
    {
        jsonxx::Object* o;
        if (v.as(o) && o->size() == 2 && o->has("first") && o->has("second"))
        {
            int first;
            std::string second;
            return (*o)["first"].as(first) && first == 25 &&
                   (*o)["second"].as(second) && second == "abc";
        }

        return false;
    } );

#undef TEST_SUCCESS

#define TEST_FAILURE(s) { \
    assert(jsonxx::parse(s) == NULL); \
}
    TEST_FAILURE("abc");
    TEST_FAILURE("3abc");
    TEST_FAILURE("3.abc");
    TEST_FAILURE("3.1abc");
    TEST_FAILURE("tru");
    TEST_FAILURE("fals");
    TEST_FAILURE("nul");
    TEST_FAILURE("(");
    TEST_FAILURE(")");
    TEST_FAILURE("{");
    TEST_FAILURE("}");
    TEST_FAILURE("[1, 2, 3, 4,]");
    TEST_FAILURE("{\"one\":1, \"two\":2, \"three\"3,}");;
    TEST_FAILURE("[1, 2, 3, 4}");
    TEST_FAILURE("{\"one\":1, \"two\":2, \"three\"3]");;
#undef TEST_FAILURE
}


int main()
{
    testStringInput();
    testStreamInput();
    testValues();

    return 0;
}
