#ifndef _JSONXX_VALUE_HANDLER_H_
#define _JSONXX_VALUE_HANDLER_H_

#include "Data.h"
#include "Parser.h"

#include <memory>
#include <stack>

namespace jsonxx
{

    /**
     * A ParseHandler implementation that generates Value objects
     * based on the parsed input
     */
    class ValueHandler : public ParseHandler
    {
    public:

        std::unique_ptr<Value> value() { return std::move(rootValue); }

        void onInteger(const std::string& name, int value);
        void onInteger(int value);
        void onDouble(const std::string& name, double value);
        void onDouble(double value);
        void onString(const std::string& name, const std::string& value);
        void onString(const std::string& value);
        void onBoolean(const std::string& name, bool value);
        void onBoolean(bool value);
        void onNull(const std::string& name);
        void onNull();

        void arrayStart(const std::string& name);
        void arrayStart();
        void arrayEnd();

        void objectStart(const std::string& name);
        void objectStart();
        void objectEnd();

    private:

        template <typename T>
        void addToObject(std::string name, T value)
        {
            Object* parent;
            if (valueStack.top().second->as(parent))
                parent->insert(name, value);
        }

        template <typename T>
        void addToArray(T value)
        {
            Array* parent;
            if (!valueStack.empty() &&
                valueStack.top().second->as(parent))
            {
                parent->add(value);
            }
        }

        void endStructure();

        std::stack<std::pair<std::unique_ptr<std::string>, 
                             std::unique_ptr<Value> > > valueStack;
        
        std::unique_ptr<Value> rootValue;
    };

}

#endif

