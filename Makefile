LIB := libjsonxx.a

CXXFLAGS := -Werror -O2
CXXFLAGS += -std=c++11 -stdlib=libc++

OBJECTS := 
OBJECTS += Parser.o
OBJECTS += Data.o
OBJECTS += ValueHandler.o

libjsonxx.a: $(OBJECTS)
	ar -r $@ $^

all: $(LIB) 

TESTS := 
TESTS += test/testparser
TESTS += test/testgenerate
$(TESTS): $(LIB)

check: CXXFLAGS += -I.
check: LDLIBS += -L. -ljsonxx
check: $(TESTS)
	for t in $(TESTS) ; do \
		$$t ; \
	done 

clean: 
	rm -f *.o test/*.o $(TESTS) $(LIB)
