#include "ValueHandler.h"

#define endValue(v, C) { \
    if (valueStack.empty()) rootValue.reset(new C(v)); \
    else \
    { \
        Array* parent; \
        if (valueStack.top().second->as(parent)) \
            parent->add(v); \
    } \
}

namespace jsonxx
{
    void ValueHandler::onInteger(const std::string& name, int value)
    {
        addToObject(name, value);
    }

    void ValueHandler::onInteger(int value)
    {
        endValue(value, Number);
    }

    void ValueHandler::onDouble(const std::string& name, double value)
    {
        addToObject(name, value);
    }

    void ValueHandler::onDouble(double value)
    {
        endValue(value, Number);
    }

    void ValueHandler::onString(const std::string& name, const std::string& value)
    {
        addToObject(name, value);
    }

    void ValueHandler::onString(const std::string& value)
    {
        endValue(value, String);
    }

    void ValueHandler::onBoolean(const std::string& name, bool value)
    {
        addToObject(name, value);
    }

    void ValueHandler::onBoolean(bool value)
    {
        endValue(value, Boolean);
    }

    void ValueHandler::onNull(const std::string& name)
    {
        Object* parent;
        if (valueStack.top().second->as(parent))
            parent->insert(name, std::unique_ptr<Value>(new Null));
    }

    void ValueHandler::onNull()
    { 
        if (valueStack.empty()) rootValue.reset(new Null); 
        else 
        { 
            auto valueStore = std::move(valueStack.top()); 
            valueStack.pop(); 
            Array* parent;
            if ((valueStack.empty() &&
                 rootValue->as(parent)) || 
                 valueStack.top().second->as(parent))
            {
                parent->addNull(); 
            }
        } 
    }

    void ValueHandler::arrayStart(const std::string& name)
    {
        Object* parent;
        if (valueStack.top().second->as(parent))
        {
            valueStack.push(
                std::make_pair(std::unique_ptr<std::string>(new std::string(name)), 
                               std::unique_ptr<Value>(new Array)));
        }
    }

    void ValueHandler::arrayStart()
    {
        valueStack.push(std::make_pair(std::unique_ptr<std::string>(),
                                       std::unique_ptr<Value>(new Array)));
    }

    void ValueHandler::arrayEnd()
    {
        endStructure();
    }

    void ValueHandler::objectStart(const std::string& name)
    {
        Object* parent;
        if (valueStack.top().second->as(parent))
        {
            valueStack.push(
                std::make_pair(std::unique_ptr<std::string>(new std::string(name)), 
                               std::unique_ptr<Value>(new Object)));
        }
    }

    void ValueHandler::objectStart()
    {
        valueStack.push(std::make_pair(std::unique_ptr<std::string>(), 
                                       std::unique_ptr<Value>(new Object)));
    }

    void ValueHandler::objectEnd()
    {
        endStructure();
    }

    void ValueHandler::endStructure()
    {
        auto value = std::move(valueStack.top());        
        valueStack.pop();

        if (valueStack.size() > 0)
        {
            Object* parentObject;
            Array* parentArray;
            if (valueStack.top().second->as(parentObject) &&
                    value.first)
            {
                parentObject->insert(*value.first, 
                        std::move(value.second));
            }
            else if (valueStack.top().second->as(parentArray))
            {
                parentArray->add(std::move(value.second));
            }
        }
        else
        {
            rootValue = std::move(value.second);
        }
    }

}

