#include "Data.h"

#include <sstream>

namespace jsonxx
{

    std::ostream& operator<<(std::ostream& o, const Value& v)
    {
        return o << v.representation();
    }

    bool Object::as(Object*& o)
    {
        o = this;
        return true;
    }

    Value& Object::operator[](const std::string& k)
    {
        return *storage[k];
    }

    bool Object::has(const std::string& k)
    {
        return storage.find(k) != storage.end();
    }

    void Object::insert(const std::string& key, std::unique_ptr<Value> value)
    {
        if (value) storage.insert(make_pair(key, std::move(value)));
    }

    void Object::insert(const std::string& key, const std::string& value)
    {
        insert(key, std::unique_ptr<Value>(new String(value)));
    }

    void Object::insert(const std::string& key, int value)
    {
        insert(key, std::unique_ptr<Value>(new Number(value)));
    }

    void Object::insert(const std::string& key, double value)
    {
        insert(key, std::unique_ptr<Value>(new Number(value)));
    }

    void Object::insert(const std::string& key, bool value)
    {
        insert(key, std::unique_ptr<Value>(new Boolean(value)));
    }

    void Object::insertNull(const std::string& key)
    {
        insert(key, std::unique_ptr<Value>(new Null));
    }

    std::string Object::representation() const
    {
        std::ostringstream output;
        output << "{";

        for (std::map<std::string, std::unique_ptr<Value> >::const_iterator i=storage.begin();
             i != storage.end();
             ++i)
        {
            String key(i->first);
            output << key << ":" << *i->second << ",";
        }

        std::string result = output.str();
        if (*result.rbegin() == ',') // remove trailing ,
            result = result.substr(0, result.length()-1); 
        result += "}";
        return result; 
    }

    bool Array::as(Array*& a)
    {
        a = this;
        return true;
    }

    Value& Array::operator[](unsigned int i)
    {
        return *storage[i];
    }

    void Array::add(std::unique_ptr<Value> newValue)
    {
        storage.push_back(std::move(newValue));
    }

    void Array::add(const std::string& value)
    {
        storage.push_back(std::unique_ptr<Value>(new String(value)));
    }

    void Array::add(int value)
    {
        storage.push_back(std::unique_ptr<Value>(new Number(value)));
    }

    void Array::add(double value)
    {
        storage.push_back(std::unique_ptr<Value>(new Number(value)));
    }

    void Array::add(bool value)
    {
        storage.push_back(std::unique_ptr<Value>(new Boolean(value)));
    }

    void Array::addNull()
    {
        storage.push_back(std::unique_ptr<Value>(new Null));
    }

    std::string Array::representation() const
    {
        std::ostringstream output;
        output << '[';
        for (std::vector<std::unique_ptr<Value> >::const_iterator i = storage.begin();
             i != storage.end();
             ++i)
        {
            output << **i << ",";
        }

        std::string result = output.str();
        if (*result.rbegin() == ',') // remove trailing ,
            result = result.substr(0, result.length()-1); 
        result += ']';
        return result;
    }

    Number::Number(int i)
        : storage({Storage::INT, {.intValue=i}})
    {
    }

    Number::Number(double d)
        : storage({Storage::DOUBLE, {.doubleValue=d}})
    {
    }

    bool Number::as(Number*& n)
    {
        n = this;
        return true;
    }

    bool Number::as(int& n)
    {
        bool result = storage.type==Storage::INT;
        if (result) n = storage.value.intValue;
        return result;
    }

    bool Number::as(double& n)
    {
        bool result = storage.type==Storage::DOUBLE;
        if (result) n = storage.value.doubleValue;
        return result;
    }


    int Number::asInt() const
    {
        return storage.type==Storage::INT?
                   storage.value.intValue:
                   storage.value.doubleValue;
    }

    double Number::asDouble() const
    {
        return storage.type==Storage::INT?
                   storage.value.intValue:
                   storage.value.doubleValue;
    }

    std::string Number::representation() const
    {
        std::ostringstream output;

        if (storage.type == Storage::INT)
        {
            output << storage.value.intValue;
        }
        else
        {
            output << storage.value.doubleValue;
        }

        return output.str();
    }

    bool Boolean::as(bool& b)
    {
        b = storage;
        return true;
    }

    std::string Boolean::representation() const
    {
        return storage?"true":"false";
    }

    String::String(std::string s)
        : storage(s)
    {
    }

    bool String::as(std::string& s)
    {
        s = storage;
        return true;
    }

    std::string String::representation() const
    {
        std::ostringstream output;
        output << "\"";

        for (char c : storage)
        {
            std::string next(1, c);
            // JSON escaping rules 
            switch (c)
            {
            case '\b':
                next = "\\b";
                break;
            case '\f':
                next = "\\f";
                break;
            case '\n':
                next = "\\n";
                break;
            case '\r':
                next = "\\r";
                break;
            case '\t':
                next = "\\t";
                break;
            case '\\':
                next = "\\\\";
                break;
            default:
                break;
            }

            output << next;
        }

        output << "\"";
        return output.str();
    }

}

